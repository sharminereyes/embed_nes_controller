`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: KU Leuven
// Engineer: Sharmine Catherine Reyes
//           Dai Hung Tran
// 
// Create Date: 14.10.2022 19:19:07
// Design Name: 
// Module Name: LED_Driver
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module LED_Driver(
    input  wire [7:0] iState,
    output wire [7:0] oLeds
    );
    
    assign oLeds = ~iState;
    
    
endmodule
