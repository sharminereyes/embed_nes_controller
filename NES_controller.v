`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: KU Leuven
// Engineer: Sharmine Catherine Reyes
//           Dai Hung Tran
// 
// Create Date: 01.10.2022 20:40:14
// Design Name: 
// Module Name: NES_controller
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module NES_controller(
    input        iClk,
    input        iRst,
    input        iNES_Data,
    output       oNES_Latch,
    output       oNES_Clock,
    output [7:0] oState,
    output       oReady
    );
    
    // State definition
    localparam sIdle   = 4'h0;
    localparam sLatch  = 4'h1;
    localparam sReadA  = 4'h2;
    localparam sReadB  = 4'h3;
    localparam sSelect = 4'h4;
    localparam sStart  = 4'h5;
    localparam sUp     = 4'h6;
    localparam sDown   = 4'h7;
    localparam sLeft   = 4'h8;
    localparam sRight  = 4'h9;
    localparam sReady  = 4'hA;
    localparam sWait   = 4'hB;

    // counter parameters (6us, 12us, 20ms)
    parameter pTIMER_6us  = 9'd149;      // 1/25MHz = 40ns; 40ns*150=6us    (8bits)
    parameter pTIMER_12us = 9'd299;      // 1/25MHz = 40ns; 40ns*300=12us   (9bits)
    parameter pTIMER_20ms = 19'd499_999; // 1/25MHz = 40ns; 40ns*500_000=20ms (19bits)
    
    reg [18:0] rCount;                   // 20ms counter
    
    reg [3:0] rFSM_Current;              // state machine registers 
    reg [3:0] rFSM_Next;
    reg [7:0] rState;
    
    // other registers
    reg  rNES_Latch;
    reg  rNES_Clock;
    reg  rReady;
    wire wCount_6us_h;                   // 6us  counter indicator
    wire wCount_12us_h;                  // 12us counter indicator
    wire wCount_20ms_h;                  // 20ms counter indicator
    wire wStateChange_h;                   // Indicator of state transition 
  
  
    // counters
    assign wStateChange_h = (rFSM_Current != rFSM_Next) ? 1'b1: 1'b0;
    
    always@(posedge iClk) begin
        if (iRst)                rCount <= 19'd0;
        else if (wStateChange_h) rCount <= 19'd0; 
        else                     rCount <= rCount + 19'd1;
    end
     
    assign wCount_12us_h = (rCount == pTIMER_12us);
    assign wCount_6us_h  = (rCount == pTIMER_6us);
    assign wCount_20ms_h = (rCount == pTIMER_20ms);
    
    // State register
    always@(posedge iClk) begin
        if(iRst)  rFSM_Current <= sIdle;
        else      rFSM_Current <= rFSM_Next;
    end
    
    // Next state logic
    always@* begin
        case (rFSM_Current)
            sIdle:
											rFSM_Next <= sLatch;
            sLatch:
				     if (wCount_12us_h)     rFSM_Next <= sReadA;
                     else                   rFSM_Next <= sLatch;
            sReadA:  
					 if (wCount_6us_h)      rFSM_Next <= sReadB;
                     else                   rFSM_Next <= sReadA;
            sReadB:  
				     if (wCount_12us_h)     rFSM_Next <= sSelect;
                     else                   rFSM_Next <= sReadB;
            sSelect: 
					 if (wCount_12us_h)     rFSM_Next <= sStart;
                     else                   rFSM_Next <= sSelect;
            sStart:  
					 if (wCount_12us_h)     rFSM_Next <= sUp;
                     else                   rFSM_Next <= sStart;
            sUp:     
					 if (wCount_12us_h)     rFSM_Next <= sDown;
                     else                   rFSM_Next <= sUp;
            sDown:   
					 if (wCount_12us_h)     rFSM_Next <= sLeft;
                     else                   rFSM_Next <= sDown;
            sLeft:   
					 if (wCount_12us_h)     rFSM_Next <= sRight;
                     else                   rFSM_Next <= sLeft;
            sRight:  
					 if (wCount_12us_h)     rFSM_Next <= sReady;
                     else                   rFSM_Next <= sRight;            
            sReady:  
					 if (wCount_12us_h)     rFSM_Next <= sWait;
                     else                   rFSM_Next <= sReady;
            sWait:   
					 if (wCount_20ms_h)     rFSM_Next <= sIdle;
                     else                   rFSM_Next <= sWait;
            default: 
											rFSM_Next <= sIdle;
        endcase
    end
    
    // output oNES_Latch
    always@* begin
        case (rFSM_Current)
            sLatch:  rNES_Latch = 1'b1;
            default: rNES_Latch = 1'b0;
        endcase
    end
    
    // output oNES_Clock
    always@* begin
        if      ((rFSM_Current == sReadB ) && (rCount <= pTIMER_6us))
            rNES_Clock = 1'b1;
        else if ((rFSM_Current == sSelect) && (rCount <= pTIMER_6us))
            rNES_Clock = 1'b1;
        else if ((rFSM_Current == sStart ) && (rCount <= pTIMER_6us))
            rNES_Clock = 1'b1;
        else if ((rFSM_Current == sUp    ) && (rCount <= pTIMER_6us))
            rNES_Clock = 1'b1;
        else if ((rFSM_Current == sDown  ) && (rCount <= pTIMER_6us))
            rNES_Clock = 1'b1;
        else if ((rFSM_Current == sLeft  ) && (rCount <= pTIMER_6us))
            rNES_Clock = 1'b1;
        else if ((rFSM_Current == sRight ) && (rCount <= pTIMER_6us))
            rNES_Clock = 1'b1;
        else if ((rFSM_Current == sReady ) && (rCount <= pTIMER_6us))
            rNES_Clock = 1'b1;
         else
            rNES_Clock = 1'b0; 
    end
    
    // output oReady
    always @* begin
        case (rFSM_Current)
            sReady:  rReady = 1'b1;
            default: rReady = 1'b0;
        endcase
    end
    
    // output oState    
    always@(posedge iClk) begin
        if      (iRst)                    rState[7:0] <= 8'hFF;
        else if (rFSM_Current == sReadA ) rState[0]   <= iNES_Data;
        else if (rFSM_Current == sReadB ) rState[1]   <= iNES_Data;
        else if (rFSM_Current == sSelect) rState[2]   <= iNES_Data;
        else if (rFSM_Current == sStart ) rState[3]   <= iNES_Data; 
        else if (rFSM_Current == sUp    ) rState[4]   <= iNES_Data;
        else if (rFSM_Current == sDown  ) rState[5]   <= iNES_Data;
        else if (rFSM_Current == sLeft  ) rState[6]   <= iNES_Data;
        else if (rFSM_Current == sRight ) rState[7]   <= iNES_Data; 
        else                              rState[7:0] <= rState[7:0];
     end
    
    // output of NES_controller
    assign oNES_Latch = rNES_Latch;
    assign oNES_Clock = rNES_Clock;
    assign oReady     = rReady;
    assign oState     = rState;
    
endmodule
