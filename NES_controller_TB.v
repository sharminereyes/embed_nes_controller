`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.10.2022 21:09:24
// Design Name: 
// Module Name: NES_controller_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module NES_controller_TB;
    reg          rClk;
    reg          rRst;
    reg          rNES_Data;
    wire         wNES_Latch;
    wire         wNES_Clock;
    wire [7:0]   wState;
    wire         wReady;
    
    // instantiation
    NES_controller NES_controller_INST(
        .iClk       (rClk),
        .iRst       (rRst),
        .iNES_Data  (rNES_Data),
        .oNES_Latch (wNES_Latch),
        .oNES_Clock (wNES_Clock),
        .oState     (wState),
        .oReady     (wReady)
    );
    
    // definition of clock period
    localparam T = 40; //1/25MHz = 40ns
    
    parameter pTIMER_6us  = 8'd150;      // 1/25MHz = 40ns; 40ns*150=6us 
    parameter pTIMER_12us = 9'd300;      // 1/25MHz = 40ns; 40ns*300=12us 
    parameter pTIMER_20ms = 19'd500_000; // 1/25MHz = 40ns; 40ns*500_000=20ms
    
    
    // generation of clock signal
    always begin
        rClk = 1'b1;
        #(T/2);
        rClk = 1'b0;
        #(T/2);
    end
    
    // stimulus generator
    initial begin
        //initial reset
        rRst      = 1'b0;
        rNES_Data = 1'b0;
        #(T);
        rRst      = 1'b1;
        #(T);
        rRst      = 1'b0;
        #(T/2);
        
        //pattern     
        #(pTIMER_12us*T); //sLatch
        rNES_Data = 1'b1;
        #(pTIMER_12us*T);
        rNES_Data = 1'b0;
        #(pTIMER_12us*T);
        rNES_Data = 1'b0;
        #(pTIMER_12us*T);
        rNES_Data = 1'b1;
        #(pTIMER_12us*T);        
        rNES_Data = 1'b1;
        #(pTIMER_12us*T);
        rNES_Data = 1'b0;
        #(pTIMER_12us*T);
        rNES_Data = 1'b0;
        #(pTIMER_12us*T);
        rNES_Data = 1'b1;                    
        #(pTIMER_20ms*T);
     
        //pattern
        #(pTIMER_12us*T); //sLatch   
        rNES_Data = 1'b0;
        #(pTIMER_12us*T);
        rNES_Data = 1'b1;
        #(pTIMER_12us*T);
        rNES_Data = 1'b1;
        #(pTIMER_12us*T);
        rNES_Data = 1'b0;
        #(pTIMER_12us*T);        
        rNES_Data = 1'b0;
        #(pTIMER_12us*T);
        rNES_Data = 1'b1;
        #(pTIMER_12us*T);
        rNES_Data = 1'b1;
        #(pTIMER_12us*T);
        rNES_Data = 1'b0;                    
        #(T);
        #(100*T);
        
        //stop simulation
        $stop;          
    end
    
endmodule
